import { createConnection, getRepository, MigrationInterface, QueryRunner } from "typeorm";
import { Theme } from "../entities/Theme";
import { Word } from "../entities/Word";

export class UndercoverWords1612625981997 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const themes = [
            {
                value: "ferme",
                words: [
                    { value: "cochon" },
                    { value: "vache" },
                    { value: "cheval" },
                    { value: "âne" },
                ]
            }, {
                value: "animaux_marins",
                words: [
                    { value: "requin" },
                    { value: "baleine" },
                    { value: "dauphin" },
                    { value: "raie" },
                ]
            }, {
                value: "assis",
                words: [
                    { value: "tabouret" },
                    { value: "canapé" },
                    { value: "chaise" },
                    { value: "banc" },
                ]
            }, {
                value: "héros_verts",
                words: [
                    { value: "Hulk" },
                    { value: "Green-lantern" },
                    { value: "Le Bouffon Vert" },
                    { value: "Loki" },
                ]
            }
        ]

        createConnection().then(() => {
            const themeRepository = getRepository(Theme);
            const wordRepository = getRepository(Word);

            for (let theme of themes) {
                const savedTheme = themeRepository.create(theme);
                theme.words.forEach(word => {
                    wordRepository.create(new Word(word.value, savedTheme.id));
                })
            }
        }
        )

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
