import SOCKET_TOPIC from "../models/SOCKET_TOPIC";
import { SocketUser } from "../models/socketUser.model";
import { User } from "../models/user.model";
import { Lobby } from "../play/lobby";
import { Game } from "../play/game";
import { Server, Socket } from "socket.io";
import { LobbyService } from "./lobby.service";
import { GameService } from "./game.service";
import * as jwt from "jsonwebtoken";
import config from "../config/config";
import { Parameters } from "../play/parameters";
import { timer } from "rxjs";

export class SocketService {

    public static io: Server;

    public static userIdsByRoom: Map<string, string[]> = new Map();
    public static users: SocketUser[] = [];

    constructor(server) {
        SocketService.io = new Server().listen(server, {
            cors: {
                origin: "*",
                methods: ["GET", "POST"],
            },
            pingInterval: 5000,
            pingTimeout: 60000,
        });
        this.init();
    }

    public init = () => {

        SocketService.io.on("connection", (socket: Socket) => {
            console.log("client connected:", socket.id);

            let socketUser: SocketUser;
            socket.once(SOCKET_TOPIC.CLIENT.CONNECT_USER, (token: string) => {
                const jwtPayload = jwt.verify(token, config.jwtSecret);
                const userId = jwtPayload["id"];
                const username = jwtPayload["username"];
                const user = new User(userId, username);
                socketUser = SocketService.connectUser(user, socket);
                console.log("user connected: " + username + " - " + userId);
                let user_room = "";
                // once a client has connected, we expect to get a ping from them saying what room they want to CREATE a Room
                socketUser.socket.on(SOCKET_TOPIC.CLIENT.LOBBY.CREATE, (isPrivate: boolean) => {
                    const room: string = SocketService.createRoom();
                    SocketService.setUsersInRoom(room, [socketUser]);
                    socketUser.socket.join(room);
                    console.log(username + " joined room " + room);
                    LobbyService.createLobby(room, isPrivate);
                    const users: User[] = SocketService.getUsersByRoom(room);
                    socketUser.socket.emit(SOCKET_TOPIC.SERVER.LOBBY.JOINED, room, users);
                    SocketService.sendLobbyStateToRoom(room, users);
                    //SocketService.sendLobbyStateToRoom(room);
                    console.log("room created:", room);
                    user_room = room;
                });
                // once a client has connected, we expect to get a ping from them saying what room they want to JOIN a Room
                socketUser.socket.on(SOCKET_TOPIC.CLIENT.LOBBY.JOIN, (room: string) => {
                    const lobby: Lobby = LobbyService.lobbies.get(room);
                    if (lobby && lobby.isOpen) {
                        if (SocketService.getUsersByRoom(room).length < Parameters.maxPlayers) {
                            SocketService.setUsersInRoom(room, [socketUser]);
                            socketUser.socket.join(room);
                            console.log(username + " joined room " + room);
                            const users: User[] = SocketService.getUsersByRoom(room);
                            socketUser.socket.emit(SOCKET_TOPIC.SERVER.LOBBY.JOINED, room, users);
                            SocketService.sendLobbyStateToRoom(room, users);
                            user_room = room;
                        } else {
                            socketUser.socket.emit(SOCKET_TOPIC.SERVER.LOBBY.JOIN_ERROR, "The lobby you tried to join is full.");
                        }
                    } else {
                        socketUser.socket.emit(SOCKET_TOPIC.SERVER.LOBBY.JOIN_ERROR, "The lobby you tried to join does not exist.");
                    }
                });

                socketUser.socket.on(SOCKET_TOPIC.CLIENT.LOBBY.FIND, () => {
                    const lobby: Lobby = LobbyService.findRandomPublicLobby();
                    if (lobby != null && lobby.isOpen) {
                        SocketService.setUsersInRoom(lobby.room, [socketUser]);
                        socketUser.socket.join(lobby.room);
                        console.log(username + " joined room " + lobby.room);
                        const users: User[] = SocketService.getUsersByRoom(lobby.room);
                        socketUser.socket.emit(SOCKET_TOPIC.SERVER.LOBBY.JOINED, lobby.room, users);
                        SocketService.sendLobbyStateToRoom(lobby.room, users);
                        //SocketService.sendLobbyStateToRoom(lobby.room);
                        user_room = lobby.room;
                    } else {
                        socketUser.socket.emit(SOCKET_TOPIC.SERVER.LOBBY.JOIN_ERROR, "No public lobbies available, create one or retry.");
                    }
                });

                socketUser.socket.on(SOCKET_TOPIC.CLIENT.LOBBY.START_GAME, (room: string) => {
                    console.log("=> the game will begin for the room", room);
                    SocketService.io.sockets.in(room).emit(SOCKET_TOPIC.SERVER.LOBBY.START_GAME);
                    timer(5000).subscribe({
                        next: () => {
                            LobbyService.getLobbyByRoom(room).startGame();
                        }
                    })
                });

                // Leave Room 
                socketUser.socket.on(SOCKET_TOPIC.CLIENT.LOBBY.LEAVE, (room: string) => {
                    if (room) {
                        console.log(SocketService.userIdsByRoom);
                        const users = SocketService.userIdsByRoom.get(room);
                        if (users != null) {
                            const index = users.findIndex((userId) => userId === socketUser.id);
                            if (index > -1) {
                                SocketService.userIdsByRoom.get(room).splice(index, 1);
                                socketUser.socket.leave(room);
                                if (SocketService.userIdsByRoom.get(room).length > 0) {
                                    SocketService.sendLobbyStateToRoom(room);
                                } else {
                                    SocketService.userIdsByRoom.delete(room);
                                    LobbyService.deleteLobby(room);
                                    console.log("room deleted:", room);
                                }
                            }
                        }
                    }
                });

                socketUser.socket.on(SOCKET_TOPIC.CLIENT.GAME.PLAYER_WORD, (room: string, word: string) => {
                    const game: Game = GameService.games.get(room);
                    game.onWordReceived(socketUser.id, word);
                });

                socketUser.socket.on(SOCKET_TOPIC.CLIENT.GAME.PLAYER_VOTE, (room: string, impostorVoteId: string) => {
                    const game: Game = GameService.games.get(room);
                    game.onVoteReceived(socketUser.id, impostorVoteId);
                });

                socketUser.socket.on(SOCKET_TOPIC.CLIENT.GAME.MRWHITE_GUESS, (room: string, word: string) => {
                    const game: Game = GameService.games.get(room);
                    game.onMrWhiteGuessReceived(word);
                });

                // User disconnected 
                socket.on('disconnect', () => {
                    const room = user_room;
                    if (room != "") {
                        const usersByRoom = SocketService.userIdsByRoom.get(room);
                        if (usersByRoom) {
                            const index = usersByRoom.findIndex((userId) => userId === socketUser.id);
                            if (index > -1) {
                                SocketService.userIdsByRoom.get(room).splice(index, 1);
                                socketUser.socket.leave(room);
                                if (SocketService.userIdsByRoom.get(room).length > 0) {
                                    SocketService.sendLobbyStateToRoom(room);
                                } else {
                                    SocketService.userIdsByRoom.delete(room);
                                    LobbyService.deleteLobby(room);
                                    console.log("room deleted:", room);
                                }
                            }
                        }
                    }
                    const indexToSplice = SocketService.users.findIndex((user) => user.id === socketUser.id);
                    SocketService.users.splice(indexToSplice, 1);
                    socket.disconnect();
                    console.log(socketUser.username + " disconnected");
                    console.log("rooms:", SocketService.userIdsByRoom);
                });
            });
        })
    }

    public static getUserBySocketId(socketId: string): SocketUser {
        return this.users.find((user) => user.socket.id === socketId);
    }

    public static getUserById(userId: string): SocketUser {
        return this.users.find((user) => user.id === userId);
    }

    public static getUsersByRoom(room: string): User[] {
        const users: User[] = [];
        this.userIdsByRoom.get(room).forEach((userId) => this.users.find((user) => {
            if (user.id === userId) {
                users.push(new User(user.id, user.username));
            }
        }));
        return users;
    }

    public static setUsersInRoom(room: string, users: SocketUser[]) {
        users.forEach((user) => this.userIdsByRoom.get(room).push(user.id));
    }

    public static connectUser(user: User, socket: Socket): SocketUser {
        const socketUser = new SocketUser(user.id, user.username, socket);
        this.users.push(socketUser);
        return socketUser;
    }

    public static unconnectUser(socketId: string) {
        const index = this.users.findIndex((user) => user.socket.id === socketId);
        this.users.splice(index, 1);
    }

    public static createRoom(): string {
        const room: string = this.generateRoomId();
        this.userIdsByRoom.set(room, []);
        return room;
    }

    public static sendLobbyStateToRoom(room: string, users: User[] = []) {
        if (users.length === 0) {
            users = SocketService.getUsersByRoom(room);
        }
        SocketService.io.to(room).emit(SOCKET_TOPIC.SERVER.LOBBY.STATE, users);
        console.log("room state send to room " + room + " :", users);
    }

    public static generateRoomId(): string {
        const randomId = Math.random().toString(36).replace("0.", "").substr(0, 5).toUpperCase();
        return this.userIdsByRoom.get(randomId) == null ? randomId : this.generateRoomId();
    }
}