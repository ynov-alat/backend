export class UtilsService {

    public static getRandomNumberBetween(min: number, max: number): number {
        return Math.floor(
            Math.random() * (max + 1 - min) + min,
        );
    }
}
