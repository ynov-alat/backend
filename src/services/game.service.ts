import { getRepository } from "typeorm";
import { Game } from "../play/game";
import { UtilsService } from "./utils.service";
import { Theme } from "../entities/Theme";
import { Word } from "../entities/Word";

export class GameService {
  public static games: Map<string, Game> = new Map();

  public static getWordsForTheGame = async (): Promise<Word[]> => {
    const themes = await getRepository(Theme).find({ relations: ["words"] });
    let isWordsSelected: boolean = false;
    let wordsSelected: Word[] = [];
    while (!isWordsSelected && themes.length > 0) {
      let theme: Theme = themes[UtilsService.getRandomNumberBetween(0, themes.length - 1)];

      const firstRandomWordIndex = UtilsService.getRandomNumberBetween(0, theme.words.length - 1);
      wordsSelected.push(theme.words[firstRandomWordIndex]);
      theme.words.splice(firstRandomWordIndex, 1);

      const secondRandomWordIndex = UtilsService.getRandomNumberBetween(0, theme.words.length - 1);
      wordsSelected.push(theme.words[secondRandomWordIndex]);

      return wordsSelected;
    }
  }
}
