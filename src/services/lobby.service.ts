import { Lobby } from "../play/lobby";
import { SocketService } from "./socket.service";
import { UtilsService } from "./utils.service";
import { Parameters } from "../play/parameters";

export class LobbyService {
  public static lobbies: Map<string, Lobby> = new Map();

  public static getLobbyByRoom(room: string): Lobby {
    return LobbyService.lobbies.get(room);
  }

  public static findRandomPublicLobby(): Lobby {
    const lobbies: Lobby[] = this.getNotFullLobbies();
    const lobbySelected = UtilsService.getRandomNumberBetween(0, lobbies.length);
    return lobbies[lobbySelected];
  }

  public static getNotFullLobbies(): Lobby[] {
    const gameLobbies = [];
    LobbyService.lobbies.forEach((lobby) => {
      if (Parameters.maxPlayers > SocketService.getUsersByRoom(lobby.room).length) {
        gameLobbies.push(lobby);
      }
    });
    return gameLobbies;
  }

  public static createLobby(room: string, isPrivate: boolean): Lobby {
    const lobby: Lobby = new Lobby(room, isPrivate);
    LobbyService.lobbies.set(room, lobby);
    return lobby;
  }

  public static deleteLobby(room: string) {
    LobbyService.lobbies.delete(room);
  }
}