import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import config from "../config/config";
import { User } from "../entities/User";

class users{

    static addUser = async (req: Request, res: Response) => {

    }

    static getUsers = async (req: Request, res: Response) => {

    }

    static getUser = async (req: Request, res: Response) => {

    }

    static updateUser = async (req: Request, res: Response) =>{

    }

    static deleteUser = async (req: Request, res: Response) => {

    }
    

}

export default users;