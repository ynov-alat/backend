import { Role } from "../play/game";

export class Results {
    public winners: Role[];
    public userRoles: Role[];
    public civilWord: string;
    public impostorWord: string;
    public guessMrWhite: string;

    constructor(winners: Role[], userRoles: Role[], civilWord: string, impostorWord: string, guessMrWhite: string) {
        this.winners = winners;
        this.userRoles = userRoles;
        this.civilWord = civilWord;
        this.impostorWord = impostorWord;
        this.guessMrWhite = guessMrWhite;
    }
}