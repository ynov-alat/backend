import { Socket } from "socket.io";
import { User } from "./user.model";

export class SocketUser extends User {

    public socket: Socket;

    constructor(id: string, username: string, socket: Socket) {
        super(id, username);
        this.socket = socket;
    }
}
