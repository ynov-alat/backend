export default {

    CLIENT: {
        /**Machine client se connecte au serveur*/
        CONNECTION: "CONNECTION",
        CONNECT_USER: "CLIENT/CONNECT_USER",
        LOBBY: {
            /**Demande au serveur de créer un lobby*/
            CREATE: "CLIENT/LOBBY/CREATE",
            /**Demande au serveur de rejoindre un lobby*/
            JOIN: "CLIENT/LOBBY/JOIN",
            /**Demande au serveur de partir du lobby*/
            LEAVE: "CLIENT/LOBBY/LEAVE",
            /**Demande au serveur rejoindre un lobby public*/
            FIND: "CLIENT/LOBBY/FIND",
            /** L'host lance la partie */
            START_GAME: "CLIENT/LOBBY/START_GAME",
        },
        GAME: {
            /**Le joueur envoi un mot*/
            PLAYER_WORD: "CLIENT/GAME/PLAYER_WORD",
            /**Le joueur vote celui pense être l'imposteur*/
            PLAYER_VOTE: "CLIENT/GAME/PLAYER_VOTE",
            /**Mr White envoi ce qui pense être le thème des civils*/
            MRWHITE_GUESS: "CLIENT/GAME/MR_WHITE_GUESS",
        },
    },
    SERVER: {
        LOBBY: {
            /** Les updates de la room sont envoyer (nouveau joueur, joueur qui quitte le lobby) */
            STATE: "SERVER/LOBBY/STATE",
            /**  */
            JOINED: "SERVER/LOBBY/JOINED",
            /**  */
            JOIN_ERROR: "SERVER/LOBBY/JOIN_ERROR",
            /** Le jeu ce lance */
            START_GAME: "SERVER/LOBBY/START_GAME"
        },
        GAME: {
            /** donne l'ordre des joueurs */
            PLAYERS_ORDER: "SERVER/GAME/PLAYERS_ORDER",
            /** Initialise le jeu, prend en compte les paramètres de jeu, distribue les rôle et envoi le thème des civils */
            PLAYER_THEME: "SERVER/GAME/PLAYER_THEME",
            /** Envoi le tour du joueur*/
            PLAYER_TURN: "SERVER/GAME/PLAYER_TURN",
            /** Signal à tout les joueurs que la phase des votes a commencée*/
            VOTE_TURN: "SERVER/GAME/VOTE_TURN",
            /** Diffuse à tous le vote d'un joueur à tout le monde */
            PLAYER_VOTE: "SERVER/GAME/PLAYER_VOTE",
            /** Envoi le résultat de fin de partie (Les gagnants, les perdants)*/
            RESULTS: "SERVER/GAME/RESULTS",
        },
    },
};