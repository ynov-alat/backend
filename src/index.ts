import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as helmet from "helmet";
import * as cors from "cors";
import * as http from "http";
import routes from "./routes";
import { SocketService } from "./services/socket.service";

//Connects to the Database -> then starts the express
createConnection().then(async connection => {
  init();
}).catch(error => console.log(error));

const init = () => {

  // Create a new express application instance
  const app = express();

  // Call midlewares
  app.use(express.static('.'));
  app.use(cors({ origin: '*' }));
  app.use(helmet());
  app.use(bodyParser.json());
  app.use(bodyParser.json({ limit: "50mb" }));
  app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }));

  //Set all routes from routes folder
  app.use("/", routes);

  // Create http server
  const server = http.createServer(app);

  new SocketService(server);

  app.get("/", (req, res) => res.send("Undercover is running."));

  server.listen(5000, "0.0.0.0", () => {
    console.log("Server started !");
  });

}
