import { timer, Subscription } from "rxjs";
import SOCKET_TOPIC from "../models/SOCKET_TOPIC";
import { User } from "../models/user.model";
import { Parameters } from "./parameters";
import { UtilsService } from "../services/utils.service";
import { SocketService } from "../services/socket.service";
import { GameService } from "../services/game.service";
import { Results } from "../models/results.model";
import { LobbyService } from "../services/lobby.service";

export enum Role {
    IMPOSTOR,
    CIVIL,
    MR_WHITE,
}

export class Game {

    public gameId: string;
    public parameters: Parameters;

    public players: Array<{
        userId: string,
        role: Role,
        words: string[],
        impostorVote: string,
    }> = [];

    public words: {
        impostorWord: string,
        civilWord: string,
        mrWhiteGuess: string,
    };

    public gameState = {
        currentUser: 0,
        turnNumber: 0,
    };

    private playerTimerSubscription: Subscription;
    private voteTimerSubscription: Subscription;
    private isWaitingPlayerWord = false;

    constructor(lobbyId: string, parameters: Parameters, users: User[]) {
        this.gameId = lobbyId;
        this.parameters = parameters;
        this.initGame(users);
    }

    private initGame(users: User[]) {
        this.setPlayers(users);
        this.setWords().then(() => {
            this.setRoles();
            this.sendPlayersOrder();
            this.sendPlayerWordToEach();
            this.sendPlayerTurnNotification(SOCKET_TOPIC.SERVER.GAME.PLAYER_TURN, null);
        })
    }

    /**
     * for each user, add a player
     */
    private setPlayers(users: User[]) {
        users.forEach((user) => {
            this.players.push({
                userId: user.id,
                role: null,
                words: [],
                impostorVote: null,
            });
        });
        this.players.sort(() => Math.random() - Math.random());
    }

    /**
     * set a role to each player, according to this.parameters.impostorNumber
     */
    private setRoles() {

        const playersWithNoRole = [];
        for (let i = 0; i < this.players.length; i++) {
            playersWithNoRole.push(i);
        }

        // set Impostors
        for (let i = 0; i < this.parameters.impostorNumber; i++) {
            const impostorIndex = UtilsService.getRandomNumberBetween(0, playersWithNoRole.length - 1);
            const player = this.players[playersWithNoRole[impostorIndex]];
            player.role = Role.IMPOSTOR;
            playersWithNoRole.splice(impostorIndex, 1);
        }

        // set Mr White
        const mrWhiteIndex = UtilsService.getRandomNumberBetween(0, playersWithNoRole.length - 1);
        const player = this.players[playersWithNoRole[mrWhiteIndex]];
        player.role = Role.MR_WHITE;
        playersWithNoRole.splice(mrWhiteIndex, 1);

        // set Civils
        for (let i = 0; i < playersWithNoRole.length; i++) {
            const player = this.players[playersWithNoRole[i]];
            player.role = Role.CIVIL;
        }

        if (this.players[0].role === Role.MR_WHITE) {
            const mrWhite = this.players[0];
            const randomIndex = UtilsService.getRandomNumberBetween(1, this.players.length - 1);
            this.players[0] = this.players[randomIndex];
            this.players[randomIndex] = mrWhite;
        }
    }

    /**
     * get word list in database, then select two randomly and affect them to this.words
     */
    private async setWords() {
        const words = await GameService.getWordsForTheGame();
        this.words = {
            civilWord: words[0].value,
            impostorWord: words[1].value,
            mrWhiteGuess: null,
        };
    }

    /**
  *
  */
    private sendPlayersOrder() {
        console.log("sendPlayersOrder:", this.players);
        const playerIds = [];
        this.players.forEach(player => {
            playerIds.push(player.userId);
        });
        SocketService.io.sockets.to(this.gameId).emit(SOCKET_TOPIC.SERVER.GAME.PLAYERS_ORDER, playerIds);
        console.log(SOCKET_TOPIC.SERVER.GAME.PLAYERS_ORDER + " sent to room " + this.gameId + " :", playerIds);
    }

    /**
     *
     */
    private sendPlayerWordToEach() {
        this.players.forEach((player) => {
            let word = "";
            switch (player.role) {
                case Role.CIVIL:
                    word = this.words.civilWord;
                    break;
                case Role.IMPOSTOR:
                    word = this.words.impostorWord;
                    break;
                default:
                    break;
            }
            SocketService.getUserById(player.userId).socket.emit(SOCKET_TOPIC.SERVER.GAME.PLAYER_THEME, word);
            console.log(SOCKET_TOPIC.SERVER.GAME.PLAYER_THEME + ": " + word + " envoyé à " + player.userId);
        });
    }

    /**
     *
     * @param userId
     * @param word
     */
    public onWordReceived(userId: string, word: string) {

        if (word === "") {
            word = "-";
        }
        console.log("word received: " + word + " from " + userId);

        // cancel the process if the user send a word after his turn
        if (this.players.findIndex((player) => player.userId === userId) !== this.gameState.currentUser) {
            return;
        }

        this.isWaitingPlayerWord = false;
        this.playerTimerSubscription.unsubscribe();

        this.addUserWord(userId, word);
        this.updateGameState();

        if (!this.isVoteTurn()) {
            this.sendPlayerTurnNotification(SOCKET_TOPIC.SERVER.GAME.PLAYER_TURN, word);
        } else {
            console.log("players:", this.players);
            console.log("gameState:", this.gameState);

            console.log(SOCKET_TOPIC.SERVER.GAME.VOTE_TURN + " send to " + this.gameId + ", lastWord: ", word);
            SocketService.io.sockets.to(this.gameId).emit(SOCKET_TOPIC.SERVER.GAME.VOTE_TURN, word);

            this.voteTimerSubscription = timer(this.parameters.timeToVote * 1000).subscribe({
                next: () => {
                    this.calculateAndSendScores();
                }
            });
        }
    }

    /**
     *
     * @param userId
     */
    public onVoteReceived(userId: string, impostorVoteId: string) {
        this.players.find((player) => player.userId === userId).impostorVote = impostorVoteId;
        console.log(SOCKET_TOPIC.SERVER.GAME.PLAYER_VOTE + " send to " + this.gameId + ", " + userId + " as voted against ", impostorVoteId);
        SocketService.io.sockets.to(this.gameId).emit(SOCKET_TOPIC.SERVER.GAME.PLAYER_VOTE, userId, impostorVoteId);

        if (this.words.mrWhiteGuess != null) {
            let goToResult = true;
            this.players.forEach(player => {
                if (player.impostorVote == null) {
                    goToResult = false;
                }
            });
            if (goToResult) {
                this.voteTimerSubscription.unsubscribe();
                this.calculateAndSendScores();
            }
        }
    }

    public onMrWhiteGuessReceived(word: string) {
        this.words.mrWhiteGuess = word;

        if (this.words.mrWhiteGuess != null) {
            let goToResult = true;
            this.players.forEach(player => {
                if (player.impostorVote == null) {
                    goToResult = false;
                }
            });
            if (goToResult) {
                this.voteTimerSubscription.unsubscribe();
                this.calculateAndSendScores();
            }
        }
    }

    /**
     * when a word is received, change the current user and increment the turn number if a turn is over
     */
    private updateGameState() {
        this.gameState.currentUser += 1;
        if (this.gameState.currentUser >= this.players.length) {
            this.gameState.turnNumber += 1;
            this.gameState.currentUser = 0;
        }
    }

    /**
     * add word to the userId's player.words
     * @param word : word send by a user
     * @param userId: the user id of the sender
     */
    private addUserWord(userId: string, word: string) {
        this.players.find((player) => player.userId === userId).words.push(word);
    }

    /**
     * return true if it's max turn or most of the half of players are ready, and if the last player of the turn have sent a word
     */
    private isVoteTurn(): Boolean {
        return this.gameState.turnNumber >= this.parameters.maxTurn;
    }

    private async startWriteTimer(seconds: number) {

        console.log("=> write timer started");
        this.playerTimerSubscription = timer(seconds * 1000).subscribe({
            next: () => {
                console.log("=> write timer ended");
                if (this.isWaitingPlayerWord) {
                    console.log("no word received");
                    this.onWordReceived(this.players[this.gameState.currentUser].userId, "-");
                }
                return;
            },
        });
    }

    private sendPlayerTurnNotification(socketTopic: string, word: string = null) {
        let message = {
            word: word,
            currentUser: this.players[this.gameState.currentUser].userId,
            turnNumber: this.gameState.turnNumber,
        };
        console.log(socketTopic + " send to " + this.gameId + " : ", message);
        SocketService.io.sockets.to(this.gameId).emit(socketTopic, message.word, message.currentUser, message.turnNumber);
        this.isWaitingPlayerWord = true;
        this.startWriteTimer(this.parameters.timeToWrite);
    }

    private calculateAndSendScores() {
        const results: Results = new Results([], [], this.words.civilWord, this.words.impostorWord, this.words.mrWhiteGuess);

        // Si le MrWhite trouve le mot il gagne
        if (this.isMrWhiteGuessCorrect()) {
            results.winners.push(Role.MR_WHITE);
        }

        const votesByImpostor: Map<string, string[]> = new Map();

        // Initialise la liste des joueurs avec les rôles dans le resultat
        this.players.forEach((player) => {
            results.userRoles.push(player.role);

            // Si imposteur : créer un tableau qui servira à lister les votes contre lui
            if (player.role === Role.IMPOSTOR) {
                votesByImpostor.set(player.userId, []);
            }
        });

        // Récupère les votes de tous les joueurs
        this.players.forEach((player) => {
            // Si le joueur qui vote est correcte il l'ajoute dans le tableau des votes de l'imposteur voté
            if (votesByImpostor.get(player.impostorVote)) {
                votesByImpostor.get(player.impostorVote).push(player.userId);
            }
        });

        const civilMajorityNumber = Math.round(this.players.length / 2);
        let isCivilVictory = true;

        // Vérifie la disposition des votes
        // Si la majorité on des votes corrects : les civils gagnent
        // Sinon les imposteurs gagnent
        votesByImpostor.forEach((votes) => {
            if (votes.length < civilMajorityNumber) {
                isCivilVictory = false;
                return;
            }
        });

        isCivilVictory ? results.winners.push(Role.CIVIL) : results.winners.push(Role.IMPOSTOR);

        console.log(SOCKET_TOPIC.SERVER.GAME.RESULTS + " send to " + this.gameId + ", results :", results);
        SocketService.io.sockets.to(this.gameId).emit(SOCKET_TOPIC.SERVER.GAME.RESULTS, results);
        const lobby = LobbyService.getLobbyByRoom(this.gameId)
        if (lobby) {
            lobby.isOpen = true;
        }
    }

    // A AMELIORER : REMPLACER LES CARACTERES SPE PAR DES CARACTERES NORMAUX
    private isMrWhiteGuessCorrect(): boolean {
        if (this.words.mrWhiteGuess == null) return false;
        return this.words.mrWhiteGuess.trim().toLowerCase() === this.words.civilWord.toLowerCase();
    }

}