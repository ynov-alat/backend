export class Parameters {

    public static readonly minPlayers = 4;
    public static readonly maxPlayers = 10;

    public impostorNumber = 1;
    public maxTurn = 4;
    public timeToWrite = 20;
    public timeToVote = 120;

    constructor() {
    }
}