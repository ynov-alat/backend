import { Game } from "./game";
import { Parameters } from "./parameters";
import { SocketService } from ".././services/socket.service";
import { GameService } from ".././services/game.service";

export class Lobby {

    public room: string;
    public isPrivate: boolean;
    public isOpen: boolean = true;

    public game: Game;

    public parameters: Parameters;

    public readyToPlayUserIds: string[] = [];

    constructor(room: string, isPrivate: boolean) {
        this.room = room;
        this.isPrivate = isPrivate;
        this.parameters = new Parameters();
    }

    public startGame() {
        this.isOpen = false;
        this.game = new Game(this.room, this.parameters, SocketService.getUsersByRoom(this.room));
        GameService.games.set(this.room, this.game);
    }

}