import { IsNotEmpty, IsEmail } from "class-validator";
import { Entity, PrimaryGeneratedColumn, Column, Unique } from "typeorm";
import * as bcrypt from "bcryptjs";

@Entity()
@Unique(["username"])
export class User {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    @IsNotEmpty()
    username: string;

    @Column()
    @IsNotEmpty()
    password: string;

    @Column()
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @Column({ nullable: true })
    skyTheme: boolean;

    @Column({ nullable: true })
    bloodyTheme: boolean;

    @Column({ nullable: true })
    mustardTheme: boolean;

    @Column({ nullable: true })
    wineTheme: boolean;

    public hashPassword() {
        this.password = bcrypt.hashSync(this.password);
    }

    checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
        return bcrypt.compareSync(unencryptedPassword, this.password);
    }

}
