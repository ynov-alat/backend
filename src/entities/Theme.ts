import { IsNotEmpty } from "class-validator";
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Word } from "./Word";

@Entity()
export class Theme {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    @IsNotEmpty()
    value: string;

    @OneToMany(() => Word, (word) => word.theme)
    words: Word[];
}
