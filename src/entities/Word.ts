import { IsNotEmpty } from "class-validator";
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from "typeorm";
import { Theme } from "./Theme";

@Entity()
export class Word {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    @IsNotEmpty()
    value: string;

    @ManyToOne(() => Theme, { cascade: true, eager: true })
    theme: Theme;
    @JoinColumn({ name: "themeId" })
    @Column({ name: "themeId", type: "uuid", nullable: true })
    themeId: string;

    constructor(value: string, themeId: string) {
        this.value = value;
        this.themeId = themeId;
    }
}
