import { Router } from "express";

const router = Router();

// get all users
router.get("/", );

// get a user
router.get("/:id", );

// Ajouter user
router.post("/", );

// Modifier user
router.put("/:id",);

// Delete user
router.delete("/:id",);

export default router;
