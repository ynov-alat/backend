import { Router } from "express";
import auth from "./auth";
import shop from "./shop";

const routes = Router();

routes.use("/auth", auth);
routes.use("/shop", shop);

export default routes;
